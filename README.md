Description
-----------
This module exposes an extra option on formatters to show or hide fields on certain domains.
Every field in the "manage display" tab can be configured.

Installation
------------
To install this module, do the following:

1. Extract the tar ball that you downloaded from Drupal.org.

2. Upload the entire directory and all its contents to your modules directory.

Configuration
-------------
To enable and configure this module do the following:

1. Go to Admin -> Modules, and enable Domain restriction formatter

2. Go to Your entity -> Manage display and start
   configuring the fields you want to put restrictions on.